import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)
const Home=()=>import('../views/homes/Home')
const Category=()=>import('../views/class/Category')
const Cart=()=>import('../views/cars/Cart')
const Profile=()=>import('../views/profiles/Profiles')
const routes=[
    {
        path:'',
        redirect:'/home'
    },
    {
        path: '/home',
        component:Home
    },
    {
        path: '/category',
        component:Category
    },
    {
        path: '/car',
        component:Cart
    },
    {
        path: '/profile',
        component:Profile
    }
]

const router =new VueRouter({
    routes,
    mode:'history'
})

export default router